package it.unibo.oop.lab06.generics1;

import java.util.*;

public class GraphImpl<N> implements Graph<N> {
	
	private Map<N,Node> nodes;
	private Algorithm algorithm;

	public GraphImpl() {
		this.nodes = new HashMap<>();
		this.algorithm = Algorithm.BFS;
	}
	
	public GraphImpl(Algorithm algorithm) {
		this();
		this.algorithm = algorithm;
	}
	
	@Override
	public void addNode(N value) {
		this.nodes.put(value, new Node(value));
	}

	@Override
	public void addEdge(N source, N target) {
		Objects.requireNonNull(source);
		Objects.requireNonNull(target);
		this.nodes.get(source).addAdiacentNode(this.nodes.get(target));
		// De-comment to get an undirected-graph
		// this.nodes.get(target).addAdiacentNode(this.nodes.get(source));
	}

	@Override
	public Set<N> nodeSet() {
		return new HashSet<>(this.nodes.keySet());
	}

	@Override
	public Set<N> linkedNodes(N node) {
		Objects.requireNonNull(node);
		Set<N> newSet = new HashSet<>();
		for (Node element : this.nodes.get(node).getAdiacentNodes()) {
			newSet.add(element.value);
		}
		return new HashSet<N>(newSet);
	}

	@Override
	public List<N> getPath(N source, N target) {
		for (Map.Entry<N,Node> entry : this.nodes.entrySet()) {
			entry.getValue().status = NodeStatus.UNVISITED;
			entry.getValue().distance = Integer.MAX_VALUE;
			entry.getValue().previous = null;
		} 
		Node sourceNode = this.nodes.get(source);
		Node targetNode = this.nodes.get(target);
		sourceNode.status = NodeStatus.VISITED;
		sourceNode.distance = 0;
		Deque<Node> queue = new LinkedList<>();
		queue.add(sourceNode);
		while (!queue.isEmpty()) {
			Node currentNode = extractNode(queue, this.algorithm);
			for (Node adiacentNode : currentNode.getAdiacentNodes()) {
				if (adiacentNode.status == NodeStatus.UNVISITED) {
					adiacentNode.status = NodeStatus.VISITED;
					adiacentNode.distance = currentNode.distance + 1;
					adiacentNode.previous = currentNode;
					queue.add(adiacentNode);
				}
			}
			currentNode.status = NodeStatus.DONE;
		}
		List<N> path = new LinkedList<>();
		Node currentNode = targetNode;
		while (currentNode.value != source) {
			path.add(currentNode.value);
			currentNode = currentNode.previous;
		}
		path.add(source);
		Collections.reverse(path);
		return path;
	}
	
	private Node extractNode(Deque<Node> queue, Algorithm algorithm) {
		if (algorithm == Algorithm.BFS) {
			return queue.removeFirst();
		}
		else if (algorithm == Algorithm.DFS) {
			return queue.removeLast();
		}
		throw new IllegalArgumentException("Second argument is not a valid Algorithm");
	}
	
	private static enum NodeStatus {
		UNVISITED, VISITED, DONE;
	}
	
	private static enum Algorithm {
		BFS, DFS;
	}
	
	public class Node {
				
		private Collection<Node> adiacentNodes;
		private NodeStatus status;
		private int distance;
		private Node previous; 
		private final N value;

		public Node(N value) {
			this.adiacentNodes = new LinkedList<>();
			this.status = NodeStatus.UNVISITED;
			this.distance = 0;
			this.previous = null;
			this.value = value;
		}

		public Collection<Node> getAdiacentNodes() {
			return this.adiacentNodes;
		}

		public void addAdiacentNode(Node node) {
			this.adiacentNodes.add(node);
		}

	}

}
