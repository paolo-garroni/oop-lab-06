package it.unibo.oop.lab.exception2;

public class NotEnoughFundsException extends IllegalStateException {
	
	private final double amount;

	public NotEnoughFundsException(double amount) {
		super();
		this.amount = amount;
	}

	public String getMessage() {
		return "Not enough funds for this operation: " + amount;
	}

}
