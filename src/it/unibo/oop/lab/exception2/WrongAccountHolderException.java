package it.unibo.oop.lab.exception2;

import java.util.Objects;

public class WrongAccountHolderException extends IllegalStateException {
	
	private final int id;

	public WrongAccountHolderException(final int id) {
		super();
		Objects.requireNonNull(id);
		this.id = id;
	}

	public String getMessage() {
		return "Account " + this.id + " is not authorized to execute this operation";
	}
	
}
