package it.unibo.oop.lab.exception2;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * JUnit test to test
 * {@link StrictBankAccount}.
 * 
 */
public class TestStrictBankAccount {

    /**
     * Used to test Exceptions on {@link StrictBankAccount}.
     */
    @Test
    public void testBankOperations() {
        /*
         * 1) Creare due StrictBankAccountImpl assegnati a due AccountHolder a
         * scelta, con ammontare iniziale pari a 10000 e nMaxATMTransactions=10
         */
    	StrictBankAccount bankAccount1 = new StrictBankAccount(1, 10000, 10);
    	StrictBankAccount bankAccount2 = new StrictBankAccount(2, 10000, 10);
    	
    	/*
         * 2) Effetture un numero di operazioni a piacere per verificare che le
         * eccezioni create vengano lanciate soltanto quando opportuno, cioè in
         * presenza di un id utente errato, oppure al superamento del numero di
         * operazioni ATM gratuite.
         */
    	boolean failure = false;
    	try {
        	// Transactions over quota
        	for (int i=0; i<11; i++) {
        		bankAccount1.depositFromATM(1, 12);
        	}
        	// Wrong Account Holder
    		bankAccount2.withdraw(1, 500);
        	// Not Enough Funds
    		bankAccount1.withdraw(1, 30000);
    	} catch (TransactionsOverQuotaException e) {
    		assertNotNull(e);
    		failure = true;
    	} catch (WrongAccountHolderException e) {
    		assertNotNull(e);
    		failure = true;
    	} catch (NotEnoughFundsException e) {
    		assertNotNull(e);
    		failure = true;
    	} finally {
    		if (failure) {
    			fail();
    		}
    	}
    	
    }
}
