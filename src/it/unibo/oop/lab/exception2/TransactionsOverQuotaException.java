package it.unibo.oop.lab.exception2;

public class TransactionsOverQuotaException extends IllegalStateException {
	
	private final int transactions;

	public TransactionsOverQuotaException(final int transactions) {
		this.transactions = transactions;
	}

	public String getMessage() {
		return "ATM transactions maximum quota exceeded: " + this.transactions;
	}
	
}
