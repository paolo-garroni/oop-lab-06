/**
 * 
 */
package it.unibo.oop.lab.collections2;

import java.util.*;

/**
 * 
 * Instructions
 * 
 * This will be an implementation of
 * {@link it.unibo.oop.lab.collections2.SocialNetworkUser}:
 * 
 * 1) complete the definition of the methods by following the suggestions
 * included in the comments below.
 * 
 * @param <U>
 *            Specific user type
 */
public class SocialNetworkUserImpl<U extends User> extends UserImpl implements SocialNetworkUser<U> {

    /*
     * 
     * [FIELDS]
     * 
     * Define any necessary field
     * 
     * In order to save the people followed by a user organized in groups, adopt
     * a generic-type Map:
     * 
     * think of what type of keys and values would best suit the requirements
     */
	Map<String, Set<U>> followedUsersMap = new HashMap<>();

    /*
     * [CONSTRUCTORS]
     * 
     * 1) Complete the definition of the constructor below, for building a user
     * participating in a social network, with 4 parameters, initializing:
     * 
     * - firstName - lastName - username - age and every other necessary field
     * 
     * 2) Define a further constructor where age is defaulted to -1
     */

    /**
     * Builds a new {@link SocialNetworkUserImpl}.
     * 
     * @param name
     *            the user firstname
     * @param surname
     *            the user lastname
     * @param userAge
     *            user's age
     * @param user
     *            alias of the user, i.e. the way a user is identified on an
     *            application
     */
    public SocialNetworkUserImpl(final String name, final String surname, final String username) {
        super(name, surname, username, -1);
    }
    
    public SocialNetworkUserImpl(final String name, final String surname, final String username, final int userAge) {
        super(name, surname, username, userAge);
    }
    
    /*
     * [METHODS]
     * 
     * Implements the methods below
     */

	@Override
    public boolean addFollowedUser(final String circle, final U user) {
        if (this.followedUsersMap.containsKey(circle)) {
        	this.followedUsersMap.get(circle).add(user);
        }
        else {
        	Set<U> newGroup = new HashSet<>();
        	newGroup.add(user);
        	this.followedUsersMap.put(circle, newGroup);
        }
        return true;
    }

    @Override
    public Collection<U> getFollowedUsersInGroup(final String groupName) {
    	if (this.followedUsersMap.containsKey(groupName)) {
    		return new HashSet<U>(this.followedUsersMap.get(groupName));
    	}
    	else return new HashSet<U>();
    }

    @Override
    public List<U> getFollowedUsers() {
    	List<U> followersList = new LinkedList<>();
    	for (Map.Entry<String, Set<U>> entry : this.followedUsersMap.entrySet()) {
    		followersList.addAll(entry.getValue());
    	}
    	return followersList;
    }

}
