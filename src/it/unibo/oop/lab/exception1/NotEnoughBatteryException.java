package it.unibo.oop.lab.exception1;

public class NotEnoughBatteryException extends IllegalStateException {
	
	private final double batteryLevel;
	
	public NotEnoughBatteryException(final double batteryLevel) {
		super();
		this.batteryLevel = batteryLevel;
	}
	
	public String getMessage() {
		return "Current battery level " + batteryLevel * 100 + "% is not enough.";
	}
	
}
