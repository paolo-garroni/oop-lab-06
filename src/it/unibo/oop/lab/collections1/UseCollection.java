package it.unibo.oop.lab.collections1;

import java.util.*;


/**
 * Example class using {@link List} and {@link Map}.
 * 
 */
public final class UseCollection {

    private UseCollection() {
    }

    /**
     * @param s
     *            unused
     */
    public static void main(final String... s) {
        /*
         * 1) Create a new ArrayList<Integer>, and populate it with the numbers
         * from 1000 (included) to 2000 (excluded).
         */
    	List<Integer> myArrayList = new ArrayList<>();
    	for (int i=1000; i<2000; i++) {
    		myArrayList.add(i);
    	}
    	
        /*
         * 2) Create a new LinkedList<Integer> and, in a single line of code
         * without using any looping construct (for, while), populate it with
         * the same contents of the list of point 1.
         */
    	List<Integer> myLinkedList = new LinkedList<>(myArrayList);
    	
        /*
         * 3) Using "set" and "get" and "size" methods, swap the first and last
         * element of the first list. You can not use any "magic number".
         * (Suggestion: use a temporary variable)
         */
    	Integer firstElement = myArrayList.get(0);
    	Integer lastElement = myArrayList.get(myArrayList.size() - 1);
    	myArrayList.set(0, lastElement);
    	myArrayList.set(myArrayList.size() - 1, firstElement);
    	
        /*
         * 4) Using a single for-each, print the contents of the arraylist.
         */
    	for (Integer element : myArrayList) {
    		System.out.println(element);
    	}
    	
        /*
         * 5) Measure the performance of inserting new elements in the head of
         * the collection: measure the time required to add 100.000 elements as
         * first element of the collection for both ArrayList and LinkedList,
         * using the previous lists. In order to measure times, use as example
         * TestPerformance.java.
         */
    	int elems = 100000;
    	// ArrayList
    	long time = System.nanoTime();
    	for (int i=0; i<elems; i++) {
    		myArrayList.set(0, 0);
    	}
    	time = System.nanoTime() - time;
    	System.out.println("Added " + elems + " elements to ArrayList in " + time + " nanoseconds");
    	// LinkedList
    	time = System.nanoTime();
    	for (int i=0; i<elems; i++) {
    		myLinkedList.set(0, 0);
    	}
    	time = System.nanoTime() - time;
    	System.out.println("Added " + elems + " elements to LinkedList in " + time + " nanoseconds");
    	
        /*
         * 6) Measure the performance of reading 1000 times an element whose
         * position is in the middle of the collection for both ArrayList and
         * LinkedList, using the collections of point 5. In order to measure
         * times, use as example TestPerformance.java.
         */
    	elems = 1000;
    	// ArrayList
    	time = System.nanoTime();
    	for (int i=0; i<elems; i++) {
    		myArrayList.get(myArrayList.size() / 2);
    	}
    	time = System.nanoTime() - time;
    	System.out.println("Read " + elems + " elements in the middle of ArrayList in " + time + " nanoseconds");
    	// LinkedList
    	time = System.nanoTime();
    	for (int i=0; i<elems; i++) {
    		myArrayList.get(myLinkedList.size() / 2);
    	}
    	time = System.nanoTime() - time;
    	System.out.println("Read " + elems + " elements in the middle of LinkedList in " + time + " nanoseconds");
        
    	/*
         * 7) Build a new Map that associates to each continent's name its
         * population:
         * 
         * Africa -> 1,110,635,000
         * 
         * Americas -> 972,005,000
         * 
         * Antarctica -> 0
         * 
         * Asia -> 4,298,723,000
         * 
         * Europe -> 742,452,000
         * 
         * Oceania -> 38,304,000
         */
    	Map<String, Long> map = new LinkedHashMap<>();
    	map.put("Africa", 1_110_635_000L);
    	map.put("Americas", 972_005_000L);
    	map.put("Antartica", 0L);
    	map.put("Asia", 4_298_723_000L);
    	map.put("Europe", 742_452_000L);
    	map.put("Oceania", 38_304_000L);
    	System.out.println(map);
    	
        /*
         * 8) Compute the population of the world
         */
    	long worldPopulation = 0;
    	for (Map.Entry<String, Long> country : map.entrySet()) {
    		worldPopulation += country.getValue();
    	}
    	System.out.println("World population is " + worldPopulation);
    }
}
